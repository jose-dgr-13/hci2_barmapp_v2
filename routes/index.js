const express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
const client = require('mongodb').MongoClient;
var path = require('path');

router.use(bodyParser.urlencoded({
  extended: true
}));
router.use(bodyParser.json());

var url = 'mongodb://localhost:27017/barmApp2';
var db = null;

client.connect(url, (err, database) => {
  if (!err) {
    console.log('connected to database');
    db = database;
  }
});

router.post('/resultados', (req, res) => {
  var resultado = {
    presupuesto: req.body.presu,
    isComida: req.body.isFood,
    tipoComida: req.body.typeFood,
    musica: req.body.music,
    espacios: req.body.espacio,
    zona: req.body.zona,
    ambiente: req.body.ambiente
  };
  db.collection(req.body.codigo).insert(resultado, (err) => {
    if (!err) {
      res.json({
        mensaje: 'resultados ingresados',
        datos: resultado
      });
    } else {
      res.json({
        mensaje: 'error al registrar datos'
      });
    }
  });
});

module.exports = router;
