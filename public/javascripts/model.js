var bares = [{
    nombre: 'Ruta 66',
    precios: '40000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'El faro',
    precios: '40000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Cerrados',
    zona: 'Norte',
    ambient: 'Clasico'
  },
  {
    nombre: 'Joshuas',
    precios: '30000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Cerveceria de antaño',
    precios: '50000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Abiertos',
    zona: 'Norte',
    ambient: 'Clasico'
  },
  {
    nombre: 'Bourbon St',
    precios: 'more',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Norte',
    ambient: 'Clasico'
  },
  {
    nombre: 'Dtoluka',
    precios: '30000',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Norte',
    ambient: 'Clasico'
  },
  {
    nombre: 'Kabaret',
    precios: 'more',
    musica: 'Regional',
    comida: 'no',
    spaces: 'Cerrados',
    zona: 'Norte',
    ambient: 'Moderno'
  },
  {
    nombre: 'Hooters',
    precios: '40000',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Norte',
    ambient: 'Moderno'
  },
  {
    nombre: 'Mr wings',
    precios: '40000',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Absenta',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Gourmet, Fast',
    spaces: 'Abiertos',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'KKahuate',
    precios: '20000',
    musica: 'Reggeton',
    comida: 'no',
    spaces: 'Abiertos',
    zona: 'Norte',
    ambient: 'Moderno'
  },
  {
    nombre: 'El rancho de jonas',
    precios: 'more',
    musica: 'Regional',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: '9th Avenue',
    precios: '30000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'BBC',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'La Estación Food and Pub',
    precios: '30000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'La pergola',
    precios: '30000',
    musica: 'Alternativo',
    comida: 'no',
    spaces: 'Abiertos',
    zona: 'Centro',
    ambient: 'Moderno'
  },
  {
    nombre: 'Canoa',
    precios: 'more',
    musica: 'Electronica',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Abiertos',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: '4 Rocas',
    precios: '30000',
    musica: 'Electrónica',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Garden lounge',
    precios: 'more',
    musica: 'Electronica',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Abiertos',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'La fontana',
    precios: '20000',
    musica: 'Salsa',
    comida: 'no',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Quickly',
    precios: '30000',
    musica: 'Reggeton',
    comida: 'no',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Epocá',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Penelope',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Abiertos',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Bio',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Abiertos',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Buffalo wings',
    precios: '30000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Bowie',
    precios: '30000',
    musica: 'Rock',
    comida: 'no',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'The famous absurdo',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Martyns',
    precios: '30000',
    musica: 'Rock',
    comida: 'no',
    spaces: 'Cerrados',
    zona: 'Norte',
    ambient: 'Moderno'
  },
  {
    nombre: 'Rock city',
    precios: '30000',
    musica: 'Rock',
    comida: 'no',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Reggae and pop',
    precios: '30000',
    musica: 'Reggae',
    comida: 'no',
    spaces: 'abierto',
    zona: 'Norte',
    ambient: 'Moderno'
  },
  {
    nombre: 'Kalilu',
    precios: '20000',
    musica: 'Reggae',
    comida: 'no',
    spaces: 'Cerrado',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Melcon',
    precios: '40000',
    musica: 'Salsa',
    comida: 'yes',
    typeFood: 'Regional',
    spaces: 'Cerrado',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Casa Corona',
    precios: '50000',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Abiertos',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Anttonina´s Cocina Natural',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Libre',
    zona: 'Sur',
    ambient: 'Clasic'
  },
  {
    nombre: 'La maldita primavera',
    precios: '30000',
    musica: 'Baladas',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Txoko Bar',
    precios: '50000',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },{
    nombre: 'PREMIUS Raclette Grill & Bar',
    precios: '50000',
    musica: 'Regional',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Malecon',
    precios: '50000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Estacion Naranja',
    precios: '30000',
    musica: 'Baladas',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Libre',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Karaoke Bar La Tocata de Charlie',
    precios: '40000',
    musica: 'Baladas',
    comida: 'no',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Bar la Iguana',
    precios: '20000|',
    musica: 'Regional',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Kimbara',
    precios: '40000',
    musica: 'Alternativo',
    comida: 'no',
    typeFood: '',
    spaces: 'Libre',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Merlot',
    precios: '30000',
    musica: 'Rock',
    comida: 'no',
    typeFood: '',
    spaces: 'Libre',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Licores Cava Barril',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'no',
    typeFood: '',
    spaces: 'Libre',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: 'Melina Bar',
    precios: '50000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Moderno'
  },
  {
    nombre: '1800',
    precios: '50000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Bar Karaoke Be The Star Night',
    precios: '50000',
    musica: 'Baladas',
    comida: 'yes',
    typeFood: 'Salsa',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Fanaticos Sport Bar',
    precios: '50000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Libre',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Macarena Bar',
    precios: '50000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Libre',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Sport Bar',
    precios: '40000',
    musica: 'Electronica',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'La maldita primavera',
    precios: '30000',
    musica: 'Baladas',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Tintindeo',
    precios: '50000',
    musica: 'Salsa',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Pecos Bil Taberna',
    precios: '50000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'La maldita primavera',
    precios: '30000',
    musica: 'Baladas',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Barril del Pascual',
    precios: '40000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Libre',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Billar Bar Olympus',
    precios: '50000',
    musica: 'Alternativo',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'La Cerveceria',
    precios: '50000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Libre',
    zona: 'Oeste',
    ambient: 'Clasico'
  },
  {
    nombre: 'Blondie´s Restaurante Bar',
    precios: '50000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'La Terraza Bar',
    precios: '50000',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Gourmet',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Clasico'
  },
  {
    nombre: 'San Lázaro - Resto Bar',
    precios: 'more',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Sur',
    ambient: 'Clasico'
  },
  {
    nombre: 'Taberna Bellos Momentos',
    precios: '30000',
    musica: 'Regional',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Este',
    ambient: 'Clasico'
  },
  {
    nombre: 'Sellsy Show Bar',
    precios: '30000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Este',
    ambient: 'Clasico'
  },
  {
    nombre: 'Pal Bar de Zalez',
    precios: '30000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Este',
    ambient: 'Clasico'
  },
  {
    nombre: 'Video Bar Son Sonero',
    precios: '40000',
    musica: 'Salsa',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Clasico'
  },
  {
    nombre: 'Icaro Bar',
    precios: '50000',
    musica: 'Rock',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Cerrados',
    zona: 'Oeste',
    ambient: 'Moderno'
  },
  {
    nombre: 'Antony Sport Bar',
    precios: '40000',
    musica: 'Alternativo',
    comida: 'yes',
    typeFood: 'Fast',
    spaces: 'Libre',
    zona: 'Norte',
    ambient: 'Clasico'
  },
  {
    nombre: 'Barril de Floralia',
    precios: '20000',
    musica: 'Regional',
    comida: 'no',
    typeFood: '',
    spaces: 'Cerrados',
    zona: 'Norte',
    ambient: 'Clasico'
  }
];
