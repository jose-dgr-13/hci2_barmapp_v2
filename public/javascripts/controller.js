var controller = function controller(view, bares) {
  // clear DB in mongoshell
  // use [database];
  // db.dropDatabase();
  //
  // fetch(`${location.origin}/api/grupos`).then((res) => res.json()).then((res) => {
  //   if (res.mensaje == 'ok') {
  //     console.log(res.grupos);
  //   }
  // });

  view.onResults = function onResults(grupo, presupuesto, isFood, food, music, spaces, zona, ambient) {
    var params = new URLSearchParams();
    params.set('codigo', grupo);
    params.set('presu', presupuesto);
    params.set('isFood', isFood);
    params.set('typeFood', food);
    params.set('music', music);
    params.set('espacio', spaces);
    params.set('zona', zona);
    params.set('ambiente', ambient);
    fetch(`${location.origin}/api/resultados`, {
        method: 'POST',
        body: params
      })
      .then((res) => res.json())
      .then((res) => {
        view.onFiltro(grupo, presupuesto, isFood, food, music, spaces, zona, ambient);
        if (res.mensaje == 'resultados ingresados') {
          console.log(res.datos);
        } else {
          console.log('cries');
        }
      });
  };

  view.onFiltro = function onFiltro(grupo, presupuesto, isFood, food, music, spaces, zona, ambient) {
    console.log(bares);
    console.log(presupuesto, isFood, food, music, spaces, zona, ambient);
    if (isFood == 'yes') {
      var filtradoConComida = bares.filter((bars) => {
        if (!presupuesto) return true;
        return bars.precios == presupuesto;
      }).filter((zone) => {
        if (!zona) return true;
        return zone.zona == zona;
      }).filter((comida) => {
        if (!food) return true;
        return comida.typeFood == food;
      });
      view.render('result', filtradoConComida);
      console.log(filtradoConComida);
    } else {
      var filtradoSinComida = bares.filter((bars) => {
        if (!presupuesto) return true;
        return bars.precios == presupuesto;
      }).filter((zone) => {
        if (!zona) return true;
        return zone.zona == zona;
      }).filter((musica) => {
        if (!music) return true;
        return musica.musica == music;
      });
      view.render('result', filtradoSinComida);
      console.log(filtradoSinComida);
    }
  };
  view.render();
}

controller(view, bares);
