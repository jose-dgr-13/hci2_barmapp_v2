var view = {
  miGrupo: '',
  invitados: null,
  miembro: '',
  size: '',
  getLogin: function getLogin() {
    var formulario = document.createElement('div');
    return formulario;
  },

  getHome: function getHome() {
    var home = document.createElement('div');
    home.setAttribute('id', 'home');
    home.innerHTML = `
    <img src="../img/Logo.png" />
    <p>Esta aplicación web permitirá a un grupo de usuarios
tener un ábanico de opciones a la hora de escoger
un bar afin a sus necesidades</p>
<p>Únete a un grupo o puedes crearlo con tus
amigos para encontrar una alternativa ideal</p>
    <button id="crear" class='button'>Crear</button>
    <button id="unir" class='button'>Unirse</button>
    `;
    var that = this;
    home.querySelector('#crear').addEventListener('click', (e) => {
      e.preventDefault();
      this.render('formul', bares);
    });
    home.querySelector('#unir').addEventListener('click', (e) => {
      e.preventDefault();
      this.render('formul', bares);
    });
    return home;
  },

  getAllForm: function getAllForm() {
    var formulario = document.createElement('div');
    formulario.setAttribute('id', 'fomulario');
    formulario.innerHTML = `
    <form>
    <h3>¿Cuánto dinero estarías dispuesto a gastar en un bar?</h3>
    <input type='radio' name='presupuesto' value='20000'> 10.000 - 20.000 <br>
    <input type='radio' name='presupuesto' value='30000'> 20.000 - 30.000 <br>
    <input type='radio' name='presupuesto' value='40000'> 30.000 - 40.000 <br>
    <input type='radio' name='presupuesto' value='50000'> 40.000 - 50.000 <br>
    <input type='radio' name='presupuesto' value='more'> 50.000 o más... <br>
    <h3>¿Qué zona prefieres? </h3>
    <input type='radio' name='zona' value='Norte'> Norte <br>
    <input type='radio' name='zona' value='Sur'> Sur <br>
    <input type='radio' name='zona' value='Centro'> Centro <br>
    <input type='radio' name='zona' value='Este'> Este <br>
    <input type='radio' name='zona' value='Oeste'> Oeste <br>
    <h3>¿Qué tipo de musica escuchas?</h3>
    <input type='radio' name='music' value='Rock'> Rock <br>
    <input type='radio' name='music' value='Salsa'> Salsa <br>
    <input type='radio' name='music' value='Reggae'> Reggae <br>
    <input type='radio' name='music' value='Alternativo'> Alternativo <br>
    <input type='radio' name='music' value='Electrónica'> Electrónica <br>
    <input type='radio' name='music' value='Regional'> Regional <br>
    <input type='radio' name='music' value='Reggeton'> Reggeton <br>
    <input type='radio' name='music' value='Baladas'> Baladas <br>
    <h3>¿Te gusta tener la opcion de comida en un bar o solo beber?</h3>
    <input type='radio' name='isFood' value='yes'> Me gusta que haya comida <br>
    <input type='radio' name='isFood' value='no'> No, solo voy para beber <br>
    <h3>¿Qué opciones te gustaria tener de comida?</h3>
    <div id='comidas'>
    <input type='radio' name='food' value='Fast'> Rápida <br>
    <input type='radio' name='food' value='Gourmet'> Gourmet <br>
    <input type='radio' name='food' value='Regional'> Regional <br>
    </div>
    <h3>¿Qué tipo de espacios te gustan?</h3>
    <input type='radio' name='spaces' value='Abiertos'> Abiertos <br>
    <input type='radio' name='spaces' value='Cerrados'> Cerrados <br>
    <input type='radio' name='spaces' value='Libre'> Al aire libre <br>
    <h3>¿Qué tipo de ambientación te gusta mas?</h3>
    <input type='radio' name='ambient' value='Clasico'> Clásico <br>
    <input type='radio' name='ambient' value='Moderno'> Moderno <br>
    <br><input type='submit' id='enviar' class='button'>
    </form>
    `;
    var that = this;
    formulario.querySelector('form').addEventListener('submit', (e) => {
      e.preventDefault();
      that.onResults('A123456', e.target.presupuesto.value, e.target.isFood.value, e.target.food.value, e.target.music.value, e.target.zona.value, e.target.zona.value, e.target.ambient.value);
    });
    return formulario;
  },

  getBar: function getBar(thisBar) {
    var bar = document.createElement('div');
    bar.className = 'bar';
    bar.innerHTML = `
      <a href="#">
      <span>${thisBar.nombre}</span>
      <h3>${thisBar.zona}</h3>
      <div class="overlay">
      <div class="play"></div>
      </div>
      </a>
    `;
    return bar;
  },

  getBares: function getBares(bares) {
    var bars = document.createElement('div');
    bars.id = 'biblioteca';
    var that = this;
    bares.forEach(function(bar) {
      var thisBar = that.getBar(bar);
      bars.appendChild(thisBar);
    });
    return bars;
  },

  render: function(pagina, bares) {
    var container = document.getElementById('root');
    container.innerHTML = '';
    switch (pagina) {
      default: container.appendChild(this.getHome());
      break;
      case 'formul':
          container.appendChild(this.getAllForm());
        break;
      case 'wait':
          container.appendChild(this.getWaiting());
        break;
      case 'result':
          container.appendChild(this.getBares(bares));
        break;
    }
  }
};
